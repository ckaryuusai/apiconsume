<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class DataController extends Controller
{
    public function postRequest()
    {
        $client = new \GuzzleHttp\Client();
        $response = $client->request('POST', 'http://localhost:8001/api/store', [
            'form_params' => [
                'name' => 'krunal',
            ]
        ]);
        $response = $response->getBody()->getContents();
        echo '<pre>';
        print_r($response);
    }

    public function getRequest()
    {
        $client     = new \GuzzleHttp\Client();
        $request    = $client->get('https://quiet-harbor-87643.herokuapp.com/api/siswa');
        $response   = $request->getBody()->getContents();
        // var_dump($res);

        // $client = new \GuzzleHttp\Client();
        // $request = $client->get('http://localhost:8001/api/index');
        // $response = $request->getBody()->getContents();
        // echo '<pre>';
        print_r($response);
        exit;
    }
}
